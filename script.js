var catalogues = [
    [
    {
        catalogue: 'Nolife',
        url: 'http://noco.tv/editeur/1/nolife',
        familles:
        [
            {
                nom: 'Chez Marcus',
                image: 'http://media.noco.tv/family/icon/nol/128x72/cm.jpg',
                video: 15082
            },
            {
                nom: 'FAQ',
                image: 'http://media.noco.tv/family/icon/nol/128x72/faq.jpg',
                video: 22582
            },
            {
                nom: 'Critiques de jeux',
                image: 'http://media.noco.tv/family/icon/nol/128x72/cs.jpg',
                video: 19187
            }
        ]
    }
        ],[
    {
        catalogue: 'Nolife',
        url: 'http://noco.tv/editeur/1/nolife',
        familles:
        [
            {
                nom: 'Retro and Magic',
                image: 'http://media.noco.tv/family/icon/nol/128x72/rm.jpg',
                video: 6260
            },
            {
                nom: 'Les oubliés de la playhistoire',
                image: 'http://media.noco.tv/family/icon/nol/128x72/opl.jpg',
                video: null
            },
            {
                nom: 'Hidden Palace',
                image: 'http://media.noco.tv/family/icon/nol/128x72/hp.jpg',
                video: null
            }
        ]
    }],[
    {
        catalogue: 'We prod',
        url: 'http://noco.tv/editeur/24/we-productions',
        familles:
        [
            {
                nom: 'Himawari! À l\'école des ninjas',
                image: 'http://media.noco.tv/family/icon/wep/128x72/hima.jpg',
                video: null
            },
            {
                nom: 'FLAG',
                image: 'http://media.noco.tv/family/icon/wep/128x72/flag.jpg',
                video: null
            },
            {
                nom: 'Casshern Sins',
                image: 'http://media.noco.tv/family/icon/wep/128x72/cash.jpg',
                video: null
            }
        ]
    },    
        {
        catalogue: 'Wakanim',
        url: 'http://noco.tv/editeur/15/wakanim',
        familles:
        [
            {
                nom: 'Robotics; Notes',
                image: 'http://media.noco.tv/family/icon/wak/128x72/rono.jpg',
                video: null
            },
            {
                nom: 'Tsuritama',
                image: 'http://media.noco.tv/family/icon/wak/128x72/tsur.jpg',
                video: null
            },
            {
                nom: 'Que sa volonté soit faite',
                image: 'http://media.noco.tv/family/icon/wak/128x72/kami.jpg',
                video: null
            }
        ]
    }],[
    {
        catalogue: 'Olydri Studio',
        url: 'http://noco.tv/editeur/4/olydri-studio',
        familles:
        [
            {
                nom: 'Noob',
                image: 'http://media.noco.tv/family/icon/oly/128x72/no.jpg',
                video: null
            },
            {
                nom: 'Le blog de Gaea',
                image: 'http://media.noco.tv/family/icon/oly/128x72/bdg.jpg',
                video: null
            },
            {
                nom: 'Warpzone project',
                image: 'http://media.noco.tv/family/icon/oly/128x72/wzp.jpg',
                video: null
            }
        ]
    },    
        {
        catalogue: 'Guardians',
        url: 'http://noco.tv/editeur/18/guardians',
        familles:
        [
            {
                nom: 'Flander\'s Company',
                image: 'http://media.noco.tv/family/icon/gua/128x72/fl.jpg',
                video: null
            },
            {
                nom: 'Damned',
                image: 'http://media.noco.tv/family/icon/gua/128x72/damn.jpg',
                video: null
            }
        ]
    }],[
    {
        catalogue: 'Toki Media',
        url: 'http://noco.tv/editeur/9/toki-media',
        familles:
        [
            {
                nom: 'Playful Kiss',
                image: 'http://media.noco.tv/family/icon/tok/128x72/kiss.jpg',
                video: null
            },
            {
                nom: 'Cool Guys, Hot Ramen',
                image: 'http://media.noco.tv/family/icon/tok/128x72/cool.jpg',
                video: null
            },
            {
                nom: 'Lucifer',
                image: 'http://media.noco.tv/family/icon/tok/128x72/luci.jpg',
                video: null
            }
        ]
    }],[
    {
        catalogue: 'Nolife',
        url: 'http://noco.tv/editeur/1/nolife',
        familles:
        [
            {
                nom: 'Esprit Japon',
                image: 'http://media.noco.tv/family/icon/nol/128x72/ej.jpg',
                video: 22714
            },
            {
                nom: 'Tôkyô Café',
                image: 'http://media.noco.tv/family/icon/nol/128x72/tc.jpg',
                video: 7439
            },
            {
                nom: 'Japan in Motion',
                image: 'http://media.noco.tv/family/icon/nol/128x72/jm.jpg',
                video: 22762
            }
        ]
    }],[
    {
        catalogue: 'Nolife',
        url: 'http://noco.tv/editeur/1/nolife',
        familles:
        [
            {
                nom: 'OTO',
                image: 'http://media.noco.tv/family/icon/nol/128x72/ot.jpg',
                video: 11490
            },
            {
                nom: 'J-Top',
                image: 'http://media.noco.tv/family/icon/nol/128x72/jtsr.jpg',
                video: null
            }
        ]
    }],[
    {
        catalogue: 'LVL42',
        url: 'http://noco.tv/editeur/12/lbl42',
        familles:
        [
            {
                nom: 'The Guild',
                image: 'http://media.noco.tv/family/icon/lbl/128x72/gui.jpg',
                video: 12610
            }
        ]
    }]
];
function openCatalogue(nb)
{
    var items = catalogues[nb - 1];
    var msg = '';
    for(j in items)
    {
        var item = items[j];
        for(i in item.familles)
        {
            var famille = item.familles[i];
            msg += '<div class="famille">\
                    <img src="'+famille.image+'" alt="'+famille.nom+'" />\
                    <h2>'+famille.nom+'</h2>';
            if(famille.video != null)
                msg += '<a href="http://noco.tv/em/'+famille.video+'" class="lien-video">Vidéo gratuite</a>';
            msg += '</div>';
        }
        msg += '<a target="_blank" class="bouton" href="'+item.url+'">Catalogue '+item.catalogue+'</a>';
    }
    document.getElementById('categorie').innerHTML = msg;
    var ul = document.getElementById('categories');
    var lis = ul.getElementsByTagName('li');
    for(i in lis)
    {
        var li = lis[i];
        if(li.id == 'cat' + nb)
            li.className = "cat-selected";
        else
            li.className = "";
    }
}